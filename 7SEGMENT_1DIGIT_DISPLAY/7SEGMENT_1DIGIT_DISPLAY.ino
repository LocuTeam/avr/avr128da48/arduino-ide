// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6 a tlačítko na pinu PC7.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin LED diody se v tom to registru nachází na í-té pozici, tedy na pozici 6
// Pin tlačítka se v tom to registru nachází na í-té pozici, tedy na pozici 7


// Zadefinujeme makro pro čas mezi zobrazováním jednotlivých čísel
#define TIME_BETWEEN_NEXT_NUMBER 1000

/*

       A          
     -----        
  F |     | B     
    |  G  |       
     -----        
  E |     | C     
    |     |       
     -----   . H  
       D          

*/


// Zadefinujeme makra pro jednotlivé segmenty
#define SEGMENT_A PIN_PC7
#define SEGMENT_B PIN_PC6
#define SEGMENT_C PIN_PC0
#define SEGMENT_D PIN_PC3
#define SEGMENT_E PIN_PC2
#define SEGMENT_F PIN_PC4
#define SEGMENT_G PIN_PC5
#define SEGMENT_H PIN_PC1


// Nadefinujeme si pole číslic, která si zapíšeme do binární hodnoty ve formátu 0bHGFEDCBA
// Na indexu 16 nadefinujeme vypnuté segmenty
unsigned char display_hex_number[17] = {0b00111111, 0b00000110, 0b01011011, 0b01001111, 0b01100110, 0b01101101, 0b01111101, 0b00000111, 0b01111111, 0b01101111, 0b01110111, 0b01111100, 0b00111001, 0b01011110, 0b01111001, 0b01110001, 0b00000000};


void setup() {

  // Nastavíme všechny segmentové piny jako výstup
  pinMode(SEGMENT_A, OUTPUT);
  pinMode(SEGMENT_B, OUTPUT);
  pinMode(SEGMENT_C, OUTPUT);
  pinMode(SEGMENT_D, OUTPUT);
  pinMode(SEGMENT_E, OUTPUT);
  pinMode(SEGMENT_F, OUTPUT);
  pinMode(SEGMENT_G, OUTPUT);
  pinMode(SEGMENT_H, OUTPUT);

  // Nastavíme všechny segmentové piny v základu do stavu logické nuly
  digitalWrite(SEGMENT_A, LOW);
  digitalWrite(SEGMENT_B, LOW);
  digitalWrite(SEGMENT_C, LOW);
  digitalWrite(SEGMENT_D, LOW);
  digitalWrite(SEGMENT_E, LOW);
  digitalWrite(SEGMENT_F, LOW);
  digitalWrite(SEGMENT_G, LOW);
  digitalWrite(SEGMENT_H, LOW);

}

void loop() {
  
  for(unsigned char number = 0; number <= 16; number = number + 1) {

    // hodnota proměnné "number" je v rozmezí 0 - 15, hodnota 16 je v poli "" nadefinovaná jako vypnutý display, další hodnoty definované nejsou (šahali bychom někam mimo pole -> hodně špatné)

    
    // Nastavujeme jednotlivé segmenty podle bitové kombinace uložené v poli "display_hex_number", kde bitová kombinace (zapínání / vypínání segmentů) se mění podle indexu = zobrazovaná hodnota
    digitalWrite(SEGMENT_A, display_hex_number[number] & 1);
    digitalWrite(SEGMENT_B, (display_hex_number[number] >> 1) & 1);
    digitalWrite(SEGMENT_C, (display_hex_number[number] >> 2) & 1);
    digitalWrite(SEGMENT_D, (display_hex_number[number] >> 3) & 1);
    digitalWrite(SEGMENT_E, (display_hex_number[number] >> 4) & 1);
    digitalWrite(SEGMENT_F, (display_hex_number[number] >> 5) & 1);
    digitalWrite(SEGMENT_G, (display_hex_number[number] >> 6) & 1);
    digitalWrite(SEGMENT_H, (display_hex_number[number] >> 7) & 1);

    delay(TIME_BETWEEN_NEXT_NUMBER);
    
  }

}
