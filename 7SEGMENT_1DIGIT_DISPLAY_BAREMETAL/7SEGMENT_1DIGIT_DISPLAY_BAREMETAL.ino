// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6 a tlačítko na pinu PC7.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin LED diody se v tom to registru nachází na í-té pozici, tedy na pozici 6
// Pin tlačítka se v tom to registru nachází na í-té pozici, tedy na pozici 7


// Zadefinujeme makro pro čas mezi zobrazováním jednotlivých čísel
#define TIME_BETWEEN_NEXT_NUMBER 1000


/*

       A          
     -----        
  F |     | B     
    |  G  |       
     -----        
  E |     | C     
    |     |       
     -----   . H  
       D          

*/


// Zadefinujeme makro pro port segmentů
#define SEGMENT_PORTX PORTC

// Zadefinujeme makra pro jednotlivé segmenty (předpokládáme, že segmenty jsou na jednom stejném portu)
#define SEGMENT_A 7
#define SEGMENT_B 6
#define SEGMENT_C 0
#define SEGMENT_D 3
#define SEGMENT_E 2
#define SEGMENT_F 4
#define SEGMENT_G 5
#define SEGMENT_H 1


// Nadefinujeme si pole číslic, která si zapíšeme do binární hodnoty ve formátu 0bHGFEDCBA
// Na indexu 16 nadefinujeme vypnuté segmenty
unsigned char display_hex_number[17] = {0b00111111, 0b00000110, 0b01011011, 0b01001111, 0b01100110, 0b01101101, 0b01111101, 0b00000111, 0b01111111, 0b01101111, 0b01110111, 0b01111100, 0b00111001, 0b01011110, 0b01111001, 0b01110001, 0b00000000};


void setup() {

  // Nastavíme všechny segmentové piny jako výstup
  SEGMENT_PORTX.DIR = 0b11111111;

  // Nastavíme všechny segmentové piny v základu do stavu logické nuly
  SEGMENT_PORTX.OUT = 0b00000000;

}

void loop() {
  
  for(unsigned char number = 0; number <= 16; number = number + 1) {

    // hodnota proměnné "number" je v rozmezí 0 - 15, hodnota 16 je v poli "" nadefinovaná jako vypnutý display, další hodnoty definované nejsou (šahali bychom někam mimo pole -> hodně špatné)

    
    // Nastavujeme jednotlivé segmenty podle bitové kombinace uložené v poli "display_hex_number", kde bitová kombinace (zapínání / vypínání segmentů) se mění podle indexu = zobrazovaná hodnota
    
    // SEGMENT A
    if(display_hex_number[number] & 1){
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT | (1 << SEGMENT_A);
    }
    else{
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT & ~(1 << SEGMENT_A);
    }

    // SEGMENT B
    if((display_hex_number[number] >> 1) & 1){
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT | (1 << SEGMENT_B);
    }
    else{
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT & ~(1 << SEGMENT_B);
    }

    // SEGMENT C
    if((display_hex_number[number] >> 2) & 1){
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT | (1 << SEGMENT_C);
    }
    else{
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT & ~(1 << SEGMENT_C);
    }

    // SEGMENT D
    if((display_hex_number[number] >> 3) & 1){
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT | (1 << SEGMENT_D);
    }
    else{
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT & ~(1 << SEGMENT_D);
    }

    // SEGMENT E
    if((display_hex_number[number] >> 4) & 1){
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT | (1 << SEGMENT_E);
    }
    else{
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT & ~(1 << SEGMENT_E);
    }

    // SEGMENT F
    if((display_hex_number[number] >> 5) & 1){
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT | (1 << SEGMENT_F);
    }
    else{
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT & ~(1 << SEGMENT_F);
    }

    // SEGMENT G
    if((display_hex_number[number] >> 6) & 1){
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT | (1 << SEGMENT_G);
    }
    else{
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT & ~(1 << SEGMENT_G);
    }

    // SEGMENT H (DP)
    if((display_hex_number[number] >> 7) & 1){
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT | (1 << SEGMENT_H);
    }
    else{
      SEGMENT_PORTX.OUT = SEGMENT_PORTX.OUT & ~(1 << SEGMENT_H);
    }

    delay(TIME_BETWEEN_NEXT_NUMBER);
    
  }

}
