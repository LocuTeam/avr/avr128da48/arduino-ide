// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6 a tlačítko na pinu PC7.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin LED diody se v tom to registru nachází na í-té pozici, tedy na pozici 6
// Pin tlačítka se v tom to registru nachází na í-té pozici, tedy na pozici 7

/*

    DIGIT4            DIGIT3             DIGIT2           DIGIT1        

       A                 A                 A                 A          
     -----             -----             -----             -----        
  F |     | B       F |     | B       F |     | B       F |     | B     
    |  G  |           |  G  |           |  G  |           |  G  |       
     -----             -----             -----             -----        
  E |     | C       E |     | C       E |     | C       E |     | C     
    |     |           |     |           |     |           |     |       
     -----   . H       -----   . H       -----   . H       -----   . H  
       D                 D                 D                 D          

*/


#define DIGIT_DELAY_MS 2


// Zadefinujeme makra pro jednotlivé segmenty
#define SEGMENT_A PIN_PC7
#define SEGMENT_B PIN_PC6
#define SEGMENT_C PIN_PC0
#define SEGMENT_D PIN_PC3
#define SEGMENT_E PIN_PC2
#define SEGMENT_F PIN_PC4
#define SEGMENT_G PIN_PC5
#define SEGMENT_H PIN_PC1

// Zadefinujeme makra pro jednotlivé digity
#define DIGIT_1 PIN_PA7
#define DIGIT_2 PIN_PA6
#define DIGIT_3 PIN_PA5
#define DIGIT_4 PIN_PA4



// Nadefinujeme si pole číslic, která si zapíšeme do binární hodnoty ve formátu 0bHGFEDCBA
// Na indexu 16 nadefinujeme vypnuté segmenty
unsigned char display_hex_cisla[17] = {0b00111111, 0b00000110, 0b01011011, 0b01001111, 0b01100110, 0b01101101, 0b01111101, 0b00000111, 0b01111111, 0b01101111, 0b01110111, 0b01111100, 0b00111001, 0b01011110, 0b01111001, 0b01110001, 0b00000000};


void setup() {

  // Nastavíme všechny segmentové piny jako výstup
  pinMode(SEGMENT_A, OUTPUT);
  pinMode(SEGMENT_B, OUTPUT);
  pinMode(SEGMENT_C, OUTPUT);
  pinMode(SEGMENT_D, OUTPUT);
  pinMode(SEGMENT_E, OUTPUT);
  pinMode(SEGMENT_F, OUTPUT);
  pinMode(SEGMENT_G, OUTPUT);
  pinMode(SEGMENT_H, OUTPUT);

  // Nastavíme všechny segmentové piny v základu do stavu logické nuly
  digitalWrite(SEGMENT_A, LOW);
  digitalWrite(SEGMENT_B, LOW);
  digitalWrite(SEGMENT_C, LOW);
  digitalWrite(SEGMENT_D, LOW);
  digitalWrite(SEGMENT_E, LOW);
  digitalWrite(SEGMENT_F, LOW);
  digitalWrite(SEGMENT_G, LOW);
  digitalWrite(SEGMENT_H, LOW);


  // Nastavíme všechny digit piny jako výstup
  pinMode(DIGIT_1, OUTPUT);
  pinMode(DIGIT_2, OUTPUT);
  pinMode(DIGIT_3, OUTPUT);
  pinMode(DIGIT_4, OUTPUT);

  // Nastavíme všechny segmentové piny v základu do stavu logické jedničky
  digitalWrite(DIGIT_1, HIGH);
  digitalWrite(DIGIT_2, HIGH);
  digitalWrite(DIGIT_3, HIGH);
  digitalWrite(DIGIT_4, HIGH);

}

void loop() {

  unsigned short number = 1234; // Zobrazované číslo



  // Výpočet číselných hodnot na jednotlivých pozicích
  
  unsigned char num1 = number % 10; // 1234 / 10 = 123  a zbytek -> 4
  
  number /= 10; // 1234 / 10 = 123
  
  unsigned char num2 = number % 10; // 123 / 10 = 12  a zbytek -> 3

  number /= 10; // 123 / 10 = 12
  
  unsigned char num3 = number % 10; // 12 / 10 = 1  a zbytek -> 2

  number /= 10; // 12 / 10 = 1
  
  unsigned char num4 = number % 10; // 1 / 10 = 0  a zbytek -> 1
  


  // DIGIT 1

  

  // Nastavujeme jednotlivé segmenty podle bitové kombinace uložené v poli "display_hex_cisla", kde bitová kombinace se mění podle indexu = zobrazovaná hodnota
  digitalWrite(SEGMENT_A, display_hex_cisla[num1] & 1);
  digitalWrite(SEGMENT_B, (display_hex_cisla[num1] >> 1) & 1);
  digitalWrite(SEGMENT_C, (display_hex_cisla[num1] >> 2) & 1);
  digitalWrite(SEGMENT_D, (display_hex_cisla[num1] >> 3) & 1);
  digitalWrite(SEGMENT_E, (display_hex_cisla[num1] >> 4) & 1);
  digitalWrite(SEGMENT_F, (display_hex_cisla[num1] >> 5) & 1);
  digitalWrite(SEGMENT_G, (display_hex_cisla[num1] >> 6) & 1);
  digitalWrite(SEGMENT_H, (display_hex_cisla[num1] >> 7) & 1);

  // Aktivujeme digit 1
  digitalWrite(DIGIT_1, LOW);
  
  delay(DIGIT_DELAY_MS); // Necháme chvilku svítit a pak až jdeme dál
  
  // Vypínáme digit 1
  digitalWrite(DIGIT_1, HIGH);



  // DIGIT 2

  // Nastavujeme jednotlivé segmenty podle bitové kombinace uložené v poli "display_hex_cisla", kde bitová kombinace se mění podle indexu = zobrazovaná hodnota
  digitalWrite(SEGMENT_A, display_hex_cisla[num2] & 1);
  digitalWrite(SEGMENT_B, (display_hex_cisla[num2] >> 1) & 1);
  digitalWrite(SEGMENT_C, (display_hex_cisla[num2] >> 2) & 1);
  digitalWrite(SEGMENT_D, (display_hex_cisla[num2] >> 3) & 1);
  digitalWrite(SEGMENT_E, (display_hex_cisla[num2] >> 4) & 1);
  digitalWrite(SEGMENT_F, (display_hex_cisla[num2] >> 5) & 1);
  digitalWrite(SEGMENT_G, (display_hex_cisla[num2] >> 6) & 1);
  digitalWrite(SEGMENT_H, (display_hex_cisla[num2] >> 7) & 1);

  // Aktivujeme digit 2
  digitalWrite(DIGIT_2, LOW);
  
  
  delay(DIGIT_DELAY_MS); // Necháme chvilku svítit a pak až jdeme dál
  
  // Vypínáme digit 2
  digitalWrite(DIGIT_2, HIGH);



  // DIGIT 3

  // Nastavujeme jednotlivé segmenty podle bitové kombinace uložené v poli "display_hex_cisla", kde bitová kombinace se mění podle indexu = zobrazovaná hodnota
  digitalWrite(SEGMENT_A, display_hex_cisla[num3] & 1);
  digitalWrite(SEGMENT_B, (display_hex_cisla[num3] >> 1) & 1);
  digitalWrite(SEGMENT_C, (display_hex_cisla[num3] >> 2) & 1);
  digitalWrite(SEGMENT_D, (display_hex_cisla[num3] >> 3) & 1);
  digitalWrite(SEGMENT_E, (display_hex_cisla[num3] >> 4) & 1);
  digitalWrite(SEGMENT_F, (display_hex_cisla[num3] >> 5) & 1);
  digitalWrite(SEGMENT_G, (display_hex_cisla[num3] >> 6) & 1);
  digitalWrite(SEGMENT_H, (display_hex_cisla[num3] >> 7) & 1);

  // Aktivujeme digit 3
  digitalWrite(DIGIT_3, LOW);

  delay(DIGIT_DELAY_MS); // Necháme chvilku svítit a pak až jdeme dál
  
  // Vypínáme digit 3
  digitalWrite(DIGIT_3, HIGH);



  // DIGIT 4

  // Nastavujeme jednotlivé segmenty podle bitové kombinace uložené v poli "display_hex_cisla", kde bitová kombinace se mění podle indexu = zobrazovaná hodnota
  digitalWrite(SEGMENT_A, display_hex_cisla[num4] & 1);
  digitalWrite(SEGMENT_B, (display_hex_cisla[num4] >> 1) & 1);
  digitalWrite(SEGMENT_C, (display_hex_cisla[num4] >> 2) & 1);
  digitalWrite(SEGMENT_D, (display_hex_cisla[num4] >> 3) & 1);
  digitalWrite(SEGMENT_E, (display_hex_cisla[num4] >> 4) & 1);
  digitalWrite(SEGMENT_F, (display_hex_cisla[num4] >> 5) & 1);
  digitalWrite(SEGMENT_G, (display_hex_cisla[num4] >> 6) & 1);
  digitalWrite(SEGMENT_H, (display_hex_cisla[num4] >> 7) & 1);

  // Aktivujeme digit 4
  digitalWrite(DIGIT_4, LOW);

  delay(DIGIT_DELAY_MS); // Necháme chvilku svítit a pak až jdeme dál
  
  // Vypínáme digit 4
  digitalWrite(DIGIT_4, HIGH);
  

}
