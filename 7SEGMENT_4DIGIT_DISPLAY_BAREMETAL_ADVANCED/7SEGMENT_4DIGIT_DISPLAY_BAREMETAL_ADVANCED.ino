// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6 a tlačítko na pinu PC7.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin LED diody se v tom to registru nachází na í-té pozici, tedy na pozici 6
// Pin tlačítka se v tom to registru nachází na í-té pozici, tedy na pozici 7

/*

    DIGIT4            DIGIT3             DIGIT2           DIGIT1        

       A                 A                 A                 A          
     -----             -----             -----             -----        
  F |     | B       F |     | B       F |     | B       F |     | B     
    |  G  |           |  G  |           |  G  |           |  G  |       
     -----             -----             -----             -----        
  E |     | C       E |     | C       E |     | C       E |     | C     
    |     |           |     |           |     |           |     |       
     -----   . H       -----   . H       -----   . H       -----   . H  
       D                 D                 D                 D          

*/


#define DIGIT_DELAY_MS 2


// Zadefinujeme makro pro port segmentů
#define SEGMENT_PORTX PORTC

// Zadefinujeme makra pro jednotlivé segmenty (předpokládáme, že segmenty jsou na jednom stejném portu)
#define SEGMENT_A 7
#define SEGMENT_B 6
#define SEGMENT_C 0
#define SEGMENT_D 3
#define SEGMENT_E 2
#define SEGMENT_F 4
#define SEGMENT_G 5
#define SEGMENT_H 1



// Zadefinujeme makro pro port digitů
#define DIGIT_PORTX PORTA

// Zadefinujeme makra pro jednotlivé digity (předpokládáme, že digity jsou na jednom stejném portu)
#define DIGIT_1 7
#define DIGIT_2 6
#define DIGIT_3 5
#define DIGIT_4 4



// Nadefinujeme si pole číslic, která si zapíšeme do binární hodnoty ve formátu 0bHGFEDCBA
// Na indexu 16 nadefinujeme vypnuté segmenty
unsigned char display_hex_number[17] = {0b00111111, 0b00000110, 0b01011011, 0b01001111, 0b01100110, 0b01101101, 0b01111101, 0b00000111, 0b01111111, 0b01101111, 0b01110111, 0b01111100, 0b00111001, 0b01011110, 0b01111001, 0b01110001, 0b00000000};


// Nadefinujeme si pole pro 4 digity ve formátu 1 z N
// Na indexu 4 nadefinujeme vypnuté digity
unsigned char display_digit[5] = {0b00000001, 0b00000010, 0b00000100, 0b00001000, 0b00000000};



void setup() {

  // Nastavíme všechny segmentové piny jako výstup
  SEGMENT_PORTX.DIR = 0b11111111;
  
  // Nastavíme všechny segmentové piny v základu do stavu logické nuly
  SEGMENT_PORTX.OUT = 0b00000000;


  // Nastavíme všechny digit piny jako výstup
  DIGIT_PORTX.DIR = DIGIT_PORTX.DIR | (1 << DIGIT_4) | (1 << DIGIT_3) | (1 << DIGIT_2) | (1 << DIGIT_1);

  // Nastavíme všechny segmentové piny v základu do stavu logické jedničky
  DIGIT_PORTX.OUT = DIGIT_PORTX.OUT | (1 << DIGIT_4) | (1 << DIGIT_3) | (1 << DIGIT_2) | (1 << DIGIT_1);
    
}

void loop() {

  unsigned short number = 1234; // Zobrazované číslo

  for(unsigned char i = 0; i < 4; ++i){

    unsigned char number_digit = number % 10;
    number /= 10;

    
    // Nastavujeme jednotlivé segmenty podle bitové kombinace uložené v poli "display_hex_cisla", kde bitová kombinace se mění podle indexu = zobrazovaná hodnota
    SEGMENT_PORTX.OUT = (((display_hex_number[number_digit] >> 7) & 1) << SEGMENT_H) | (((display_hex_number[number_digit] >> 6) & 1) << SEGMENT_G) | (((display_hex_number[number_digit] >> 5) & 1) << SEGMENT_F) | (((display_hex_number[number_digit] >> 4) & 1) << SEGMENT_E) | (((display_hex_number[number_digit] >> 3) & 1) << SEGMENT_D) | (((display_hex_number[number_digit] >> 2) & 1) << SEGMENT_C) | (((display_hex_number[number_digit] >> 1) & 1) << SEGMENT_B) | (((display_hex_number[number_digit] >> 0) & 1) << SEGMENT_A);

    // Nastavíme správný digit v pořadí
    DIGIT_PORTX.OUT = DIGIT_PORTX.OUT & ~(((display_digit[i] >> 3) & 1) << DIGIT_4) & ~(((display_digit[i] >> 2) & 1) << DIGIT_3) & ~(((display_digit[i] >> 1) & 1) << DIGIT_2) & ~((display_digit[i] & 1) << DIGIT_1);
    
    delay(DIGIT_DELAY_MS); // Necháme chvilku svítit a pak až jdeme dál

    // Nastavíme všechny digitové piny do stavu logické jedničky
    DIGIT_PORTX.OUT = DIGIT_PORTX.OUT | (1 << DIGIT_4) | (1 << DIGIT_3) | (1 << DIGIT_2) | (1 << DIGIT_1);
    
  }

}
