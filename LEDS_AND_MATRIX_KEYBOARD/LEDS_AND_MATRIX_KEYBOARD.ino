// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6 a tlačítko na pinu PC7.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin LED diody se v tom to registru nachází na í-té pozici, tedy na pozici 6
// Pin tlačítka se v tom to registru nachází na í-té pozici, tedy na pozici 7

// Zadefinujeme makra pro LED diody charakterizující hodnotou právě zmáčknuté tlačítko maticové klávesnice -> 16 tlačítek -> 4 bity (4 LED diody)
#define LED1 PIN_PC0
#define LED2 PIN_PC1
#define LED3 PIN_PC2
#define LED4 PIN_PC3


// Maticová klávesnice - vnitřní zapojení
/*

Matrix:
        
        MK4  MK5  MK6  MK7
         |    |    |    |
  MK0 > -|----|----|----|-
  MK1 > -|----|----|----|-
  MK2 > -|----|----|----|-
  MK3 > -|----|----|----|-
         |    |    |    |


Contacts:

  MK0  MK1  MK2  MK3  MK4  MK5  MK6  MK7
   |    |    |    |    |    |    |    |  

*/


// Zadefinujeme makra pro piny maticové klávesnice

// Řádky
#define MK0 PIN_PD0
#define MK1 PIN_PD1
#define MK2 PIN_PD2
#define MK3 PIN_PD3

// Sloupce
#define MK4 PIN_PD4
#define MK5 PIN_PD5
#define MK6 PIN_PD6
#define MK7 PIN_PD7



void setup() {

  // Nastavíme piny LED diod jako výstup
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);

  // Vypneme LED diody
  digitalWrite(LED1, HIGH);
  digitalWrite(LED2, HIGH);
  digitalWrite(LED3, HIGH);
  digitalWrite(LED4, HIGH);

  // Nastavíme všechny piny maticové klávesnice jako vstupy s PULL-UP rezistory
  pinMode(MK0, INPUT_PULLUP);
  pinMode(MK1, INPUT_PULLUP);
  pinMode(MK2, INPUT_PULLUP);
  pinMode(MK3, INPUT_PULLUP);
  pinMode(MK4, INPUT_PULLUP);
  pinMode(MK5, INPUT_PULLUP);
  pinMode(MK6, INPUT_PULLUP);
  pinMode(MK7, INPUT_PULLUP);
  
}

void loop() {

  unsigned char hodnota = 0; // Proměnná držící hodnotu aktuálně zmáčknutého tlačítka na maticové klávesnici
  
  // Nastavíme první řadek jako výstupní a nastavíme mu logickou nulu (GND)
  pinMode(MK0, OUTPUT);
  digitalWrite(MK0, LOW);

  // Koukáme na piny po sloupcích na řádku jedna, jestli byly zmáčknuty. Pokud byly, nastavujeme do proměnné "hodnota" hodnotu právě zmáčknutého tlačítka
  if(digitalRead(MK4) == 0) hodnota = 1;
  else if(digitalRead(MK5) == 0) hodnota = 2;
  else if(digitalRead(MK6) == 0) hodnota = 3;
  else if(digitalRead(MK7) == 0) hodnota = 4;


  // Opětovně nastavujeme pin prvního řádku jako vstup s PULL-UP rezistorem
  pinMode(MK0, INPUT_PULLUP);

  
  // Nastavíme druhý řadek jako výstupní a nastavíme mu logickou nulu (GND)
  pinMode(MK1, OUTPUT);
  digitalWrite(MK1, LOW);

  // Koukáme na piny po sloupcích na řádku dva, jestli byly zmáčknuty. Pokud byly, nastavujeme do proměnné "hodnota" hodnotu právě zmáčknutého tlačítka
  if(digitalRead(MK4) == 0) hodnota = 5;
  else if(digitalRead(MK5) == 0) hodnota = 6; 
  else if(digitalRead(MK6) == 0) hodnota = 7;
  else if(digitalRead(MK7) == 0) hodnota = 8;


  // Opětovně nastavujeme pin druhého řádku jako vstup s PULL-UP rezistorem
  pinMode(MK1, INPUT_PULLUP);


  // Nastavíme třetí řadek jako výstupní a nastavíme mu logickou nulu (GND)
  pinMode(MK2, OUTPUT);
  digitalWrite(MK2, LOW);

  // Koukáme na piny po sloupcích na řádku tři, jestli byly zmáčknuty. Pokud byly, nastavujeme do proměnné "hodnota" hodnotu právě zmáčknutého tlačítka
  if(digitalRead(MK4) == 0) hodnota = 9;
  else if(digitalRead(MK5) == 0) hodnota = 10;
  else if(digitalRead(MK6) == 0) hodnota = 11;
  else if(digitalRead(MK7) == 0) hodnota = 12;
  

  // Opětovně nastavujeme pin třetího řádku jako vstup s PULL-UP rezistorem
  pinMode(MK2, INPUT_PULLUP);


  // Nastavíme čtvrtý řadek jako výstupní a nastavíme mu logickou nulu (GND)
  pinMode(MK3, OUTPUT);
  digitalWrite(MK3, LOW);

  // Koukáme na piny po sloupcích na řádku čtyři, jestli byly zmáčknuty. Pokud byly, nastavujeme do proměnné "hodnota" hodnotu právě zmáčknutého tlačítka
  if(digitalRead(MK4) == 0) hodnota = 13;
  else if(digitalRead(MK5) == 0) hodnota = 14;
  else if(digitalRead(MK6) == 0) hodnota = 15;
  else if(digitalRead(MK7) == 0) hodnota = 0;
  

  // Opětovně nastavujeme pin čtvrtého řádku jako vstup s PULL-UP rezistorem
  pinMode(MK3, INPUT_PULLUP);


  // Na konci vypisujeme hodnotu obsahující informaci o zmáčknutém tlačítko a převádíme do 4 bitové reprezentrace pro 4 LED diody
  digitalWrite(LED1, (hodnota & 1) ? LOW : HIGH);
  digitalWrite(LED2, ((hodnota >> 1) & 1) ? LOW : HIGH);
  digitalWrite(LED3, ((hodnota >> 2) & 1) ? LOW : HIGH);
  digitalWrite(LED4, ((hodnota >> 3) & 1) ? LOW : HIGH);
   
}
