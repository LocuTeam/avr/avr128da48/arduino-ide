// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6 a tlačítko na pinu PC7.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin LED diody se v tom to registru nachází na í-té pozici, tedy na pozici 6
// Pin tlačítka se v tom to registru nachází na í-té pozici, tedy na pozici 7

// Zadefinujeme makra pro LED diody charakterizující hodnotou právě zmáčknuté tlačítko maticové klávesnice -> 16 tlačítek -> 4 bity (4 LED diody)
#define LED1 0
#define LED2 1
#define LED3 2
#define LED4 3


// Maticová klávesnice - vnitřní zapojení
/*

Matrix:
        
        MK4  MK5  MK6  MK7
         |    |    |    |
  MK0 > -|----|----|----|-
  MK1 > -|----|----|----|-
  MK2 > -|----|----|----|-
  MK3 > -|----|----|----|-
         |    |    |    |


Contacts:

  MK0  MK1  MK2  MK3  MK4  MK5  MK6  MK7
   |    |    |    |    |    |    |    |  

*/


// Zadefinujeme makra pro piny maticové klávesnice

// Řádky
#define MK0 0
#define MK1 7
#define MK2 5
#define MK3 4

// Sloupce
#define MK4 6
#define MK5 3
#define MK6 2
#define MK7 1


void setup() {
  
  // Nastavíme piny LED diod jako výstup
  PORTC.DIR = (1 << LED4) | (1 << LED3) | (1 << LED2) | (1 << LED1);

  // Vypneme LED diody
  PORTC.OUT = (1 << LED4) | (1 << LED3) | (1 << LED2) | (1 << LED1);

  // Nastavíme všechny piny maticové klávesnice jako vstupy (použijeme externí PULL-UP rezistory)
  PORTD.DIR = 0b00000000; // Nastavení všech pinů na vstupy
  PORTD.PINCTRLUPD = 0b11111111; // Nastavení masky pro možnost nastavování chování celého registru v následujícím kroku
  PORTD.PINCONFIG = 0b00001000; // Nastavení PULL-UP rezistorů pro všechny piny na portu
  
}

void loop() {

  unsigned char hodnota = 0; // Proměnná držící hodnotu aktuálně zmáčknutého tlačítka na maticové klávesnici

  
  // Nastavíme první řadek jako výstupní a nastavíme mu logickou nulu (GND)
  PORTD.DIR = PORTD.DIR | (1 << MK0);
  PORTD.OUT = PORTD.OUT & ~(1 << MK0);

  // Koukáme na piny po sloupcích na řádku jedna, jestli byly zmáčknuty. Pokud byly, nastavujeme do proměnné "hodnota" hodnotu právě zmáčknutého tlačítka
  if(((PORTD.IN >> MK4) & 1) == 0) hodnota = 1;
  else if(((PORTD.IN >> MK5) & 1) == 0) hodnota = 2;
  else if(((PORTD.IN >> MK6) & 1) == 0) hodnota = 3;
  else if(((PORTD.IN >> MK7) & 1) == 0) hodnota = 4;


  // Opětovně nastavujeme pin prvního řádku jako vstup
  PORTD.DIR = PORTD.DIR & ~(1 << MK0);
  PORTD.PINCTRLUPD = 0b11111111;
  PORTD.PINCONFIG = 0b00001000;

  
  // Nastavíme druhý řadek jako výstupní a nastavíme mu logickou nulu (GND)
  PORTD.DIR = PORTD.DIR | (1 << MK1);
  PORTD.OUT = PORTD.OUT & ~(1 << MK1);

  // Koukáme na piny po sloupcích na řádku dva, jestli byly zmáčknuty. Pokud byly, nastavujeme do proměnné "hodnota" hodnotu právě zmáčknutého tlačítka
  if(((PORTD.IN >> MK4) & 1) == 0) hodnota = 5;
  else if(((PORTD.IN >> MK5) & 1) == 0) hodnota = 6; 
  else if(((PORTD.IN >> MK6) & 1) == 0) hodnota = 7;
  else if(((PORTD.IN >> MK7) & 1) == 0) hodnota = 8;


  // Opětovně nastavujeme pin druhého řádku jako vstup
  PORTD.DIR = PORTD.DIR & ~(1 << MK1);
  PORTD.PINCTRLUPD = 0b11111111;
  PORTD.PINCONFIG = 0b00001000;


  // Nastavíme třetí řadek jako výstupní a nastavíme mu logickou nulu (GND)
  PORTD.DIR = PORTD.DIR | (1 << MK2);
  PORTD.OUT = PORTD.OUT & ~(1 << MK2);

  // Koukáme na piny po sloupcích na řádku tři, jestli byly zmáčknuty. Pokud byly, nastavujeme do proměnné "hodnota" hodnotu právě zmáčknutého tlačítka
  if(((PORTD.IN >> MK4) & 1) == 0) hodnota = 9;
  else if(((PORTD.IN >> MK5) & 1) == 0) hodnota = 10;
  else if(((PORTD.IN >> MK6) & 1) == 0) hodnota = 11;
  else if(((PORTD.IN >> MK7) & 1) == 0) hodnota = 12;
  

  // Opětovně nastavujeme pin třetího řádku jako vstup
  PORTD.DIR = PORTD.DIR & ~(1 << MK2);
  PORTD.PINCTRLUPD = 0b11111111;
  PORTD.PINCONFIG = 0b00001000;


  // Nastavíme čtvrtý řadek jako výstupní a nastavíme mu logickou nulu (GND)
  PORTD.DIR = PORTD.DIR | (1 << MK3);
  PORTD.OUT = PORTD.OUT & ~(1 << MK3);

  // Koukáme na piny po sloupcích na řádku čtyři, jestli byly zmáčknuty. Pokud byly, nastavujeme do proměnné "hodnota" hodnotu právě zmáčknutého tlačítka
  if(((PORTD.IN >> MK4) & 1) == 0) hodnota = 13;
  else if(((PORTD.IN >> MK5) & 1) == 0) hodnota = 14;
  else if(((PORTD.IN >> MK6) & 1) == 0) hodnota = 15;
  else if(((PORTD.IN >> MK7) & 1) == 0) hodnota = 0;
  

  // Opětovně nastavujeme pin čtvrtého řádku jako vstup
  PORTD.DIR = PORTD.DIR & ~(1 << MK3);
  PORTD.PINCTRLUPD = 0b11111111;
  PORTD.PINCONFIG = 0b00001000;
  

  // Na konci vypisujeme hodnotu obsahující informaci o zmáčknutém tlačítko a převádíme do 4 bitové reprezentrace pro 4 LED diody
  unsigned char value_led1 = (hodnota & 1) ? 0 : 1;
  unsigned char value_led2 = ((hodnota >> 1) & 1) ? 0 : 1;
  unsigned char value_led3 = ((hodnota >> 2) & 1) ? 0 : 1;
  unsigned char value_led4 = ((hodnota >> 3) & 1) ? 0 : 1;
  
  PORTC.OUT = (value_led4 << LED4) | (value_led3 << LED3) | (value_led2 << LED2) | (value_led1 << LED1);
   
}
