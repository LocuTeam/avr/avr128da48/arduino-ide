// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6 a tlačítko na pinu PC7.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin LED diody se v tom to registru nachází na í-té pozici, tedy na pozici 6
// Pin tlačítka se v tom to registru nachází na í-té pozici, tedy na pozici 7


// Zadefinujeme makro LED, které bude reprezentovat náš pin PC6
#define LED PIN_PC6

// Zadefinujeme makro BUTTON, které bude reprezentovat náš pin PC7
#define BUTTON PIN_PC7

void setup() {

  pinMode(LED, OUTPUT); // Nastavíme LED diodu jako OUTPUT (výstup)
  digitalWrite(LED, HIGH); // Vypneme LED diodu

  pinMode(BUTTON, INPUT_PULLUP); // Nastavíme tlačítko jako INPUT (vstup) a zapneme vnitřní PULL-UP rezistor
  
}

void loop() {

  // Pokud je na pinu tlačítka logická nula -> tlačítko bylo zmáčknuto, pak proveď operace uvnitř výrazu if
  if(digitalRead(BUTTON) == 0){
    digitalWrite(LED, LOW); // Zapneme LED diodu
  }
  else{
    digitalWrite(LED, HIGH); // Zapneme LED diodu // Vypneme LED diodu
  }
  
}
