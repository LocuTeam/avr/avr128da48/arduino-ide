// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6 a tlačítko na pinu PC7.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin LED diody se v tom to registru nachází na í-té pozici, tedy na pozici 6
// Pin tlačítka se v tom to registru nachází na í-té pozici, tedy na pozici 7


// Zadefinujeme makro LED, které bude reprezentovat náš pin PC6 -> 6. bit na portu C
#define LED 6

void setup() {
  
  PORTC.DIR = PORTC.OUT | (1 << LED); // Nastavíme v registru PORTC.DIR 6. bit na logickou jedničku -> nastavíme LED diodu jako OUTPUT (výstup)
  PORTC.OUT = PORTC.OUT | (1 << LED); // Nastavíme ligickou jedničku na pin LED diody -> LED didoa je vypnutá

  PORTC.DIR = PORTC.DIR & ~(1 << 7); // Nastavíme v registru PORTC.DIR 7. bit na logickou nulu -> nastavíme tlačítko jako INPUT (vstup)
  PORTC.PIN7CTRL = 0b00001000;       // Nastavíme na pin tlačítka vnitřní PULL-UP rezistor -> pin bude mít pořád napětí 3.3V (díky pull-up rezistoru) -> při zmáčknutí tlačítka se napětí změní na 0V
  
}

void loop() {

  // Pokud na sedmé pozici registru PORT.IN je hodnota 0 -> tlačítko bylo zmáčknuto, pak proveď operace uvnitř výrazu if
  if(((PORTC.IN >> 7) & 1) == 0){
    PORTC.OUT = PORTC.OUT & ~(1 << LED); // Zapneme LED diodu
  }
  else{
    PORTC.OUT = PORTC.OUT | (1 << LED); // Vypneme LED diodu
  }
  
}
