// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin se v tom to registru nachází na í-té pozici, tedy na pozici 6

// Můžeme použít MAKRO PIN_PC6, což je náš pin PC6

// Zadefinujeme MAKRO LED, které bude reprezentovat náš pin PIN_PC6
#define LED PIN_PC6

void setup() {

  pinMode(LED, OUTPUT); // Nastavíme pin PC6 jako výstupní 

  digitalWrite(LED, HIGH); // Nastavíme pin PC6 do stavu logické jedničky -> "je napětí" -> LED dioda je vypnutá

}

void loop() {

  digitalWrite(LED, LOW); // Nastavení pinu PC6 do stavu logické nuly -> "není napětí" -> LED dioda je zapnutá
  
  delay(500);
  
  digitalWrite(LED, HIGH); // Nastavení pinu PAC6 do stavu logické jedničky -> "je napětí" -> LED dioda je vypnutá
  
  delay(500);
  
}
