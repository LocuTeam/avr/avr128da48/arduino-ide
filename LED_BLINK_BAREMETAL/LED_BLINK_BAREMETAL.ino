// Vyvojová deska AVR128DA48 má připojenou LED diodu na pinu PC6.
// To znamená, že jestli na daném pinu bude napětí anebo nebude napětí, bude vstupní hodnota anebo bude výstupní, můžeme ovlivňovat přes registr PORTC.
// Pin se v tom to registru nachází na í-té pozici, tedy na pozici 6

void setup() {

  PORTC.DIR = 0b01000000; // Zápisem 1 na 6. bit (pin) nastavíme daný pin 6 jako OUTPUT -> daný pin bude sloužit jako výstup (můžeme říkat, jestli má mít napětí 3.3V anebo 0V)
  PORTC.OUT = 0b01000000; // Zápisem 1 na 6. bit (pin) nastavíme logickou jedničku na pin PC6 -> "je napětí" -> dioda je vypnutá

  // Další možnost při použití bitových rotací
  // PORTC.DIR = 1 << 6;
  // PORTC.OUT = 1 << 6;
  
}

void loop() {

  // První naivní řešení -> nebude fungovat vždy

  // V registru PORTC.OUT máme hodnotu 0bx1xxxxxx
  PORTC.OUT = 0b00000000; // Vynulujeme celý registr -> vynuluje se bit na 6. pozici -> bit (pin) se nastaví do stavu logické nuly -> dioda je zapnutá
  
  delay(500); // funkce delay(miliseconds); čeká zadaný počet milisekund (program se na ní zastaví) a až zadaný počet milisekund uplyne, program jede dál
  // čekám než uplyne 500 milisekund, pak teprve pokračuju dál

  // V registru PORTC.OUT máme hodnotu 0b00000000
  PORTC.OUT = 0b11111111; // Nastaví celý registr na samé jedničky -> bit na 6. pozici se nastaví na jedničku -> bit (pin) se nastaví do stavu logické jedničky -> dioda je vypnutá

  delay(500);

  // V registru PORTC.OUT zůstalo 0b11111111




  // Lepší řešení -> bude potřeba pro další programy -> nesmíme si přepisovat celé registry!

  // V registru PORTC.OUT máme hodnotu 0b11111111
  PORTC.OUT = PORTC.OUT & 0b10111111; // Vynulujeme pouze bit na 6. pozici -> pin se nastaví do stavu logické nuly -> dioda je zapnutá

  delay(500);

  // V registru PORTC.OUT máme hodnotu 0b10111111
  PORTC.OUT = PORTC.OUT | 0b01000000; // Nastavíme pouze bit na 6. pozici do stavu logické jedničky -> pin se nastaví do stavu logické jedničky -> dioda je vypnutá

  delay(500);




  // Nejlepší řešení -> funguje vždy

  // V registru PORTC.OUT máme hodnotu 0b11111111
  PORTC.OUT = PORTC.OUT & ~(1 << 6); // PORTC.OUT & ~(1 << 6) = PORTC.OUT & ~(0b01000000) = PORTC.OUT & 0b10111111 = 0b11111111 & 0b10111111 = 0b10111111
  
  delay(500);

  // V registru PORTC.OUT máme hodnotu 0b10111111
  PORTC.OUT = PORTC.OUT | (1 << 6); // PORTC.OUT | (1 << 6) = PORTC.OUT | 0b01000000 = 0b10111111 | 0b01000000 = 0b11111111
  
  delay(500);
  
}
