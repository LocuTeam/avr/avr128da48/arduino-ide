# Arduino IDE

Gitový repozitář projektů v Arduino IDE pro AVR mikrokontroler AVR128DB48.


## DXCore

Pro práci s vývojovým prostředím Arduino IDE musíme použít externí knihovnu pro vývojové desky DXCore.

### Instalace v manažeru desek

Balíčky pro práci s deskou AVR128DB48 se mohou nasintalovat skrze manažer desek. Externí URL pro manažer desek je:

`http://drazzy.com/package_drazzy.com_index.json`

1. File -> Preferences, vložte odkaz nahoře do pole "Additional Boards Manager URLs"
2. Tools -> Boards -> Boards Manager...
3. Počkejte dokud se nenačte seznam desek
4. Vyberte "DXCore by Spence Konde" a klikněte na "Install"